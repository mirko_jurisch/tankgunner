//
//  GameScene.swift
//  TankGunner
//
//  Created by Mirko Jurisch on 04.02.17.
//  Copyright © 2017 Mirko Jurisch. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    
    let tank = SKSpriteNode(imageNamed: "Tankzerostar")
    
    let light = SKLightNode()
    
    let background = SKSpriteNode(imageNamed: "Hintergrund")
    
   // var audioURL: URL?
   //  var backgroundAudio = AVAudioPlayer()
    
    var liveCount: Int = 0
    var liveArray = [SKSpriteNode]()
    
    
    var timer6 = Timer()
    var timer7 = Timer()
    var timer8 = Timer()
    var timer9 = Timer()
    var timer10 = Timer()
    
    
    var score:Int = 0
    var scoreLabel = SKLabelNode()
    
    struct physicsCA {
        static let tankPH: UInt32 = 0b1 //1
        static let spacefirePH: UInt32 = 0b10 //2
        static let enemyPH : UInt32 = 0b100 //3
        static let baumPH : UInt32 = 0b1000 //4
    }
    
    
    // let backgroundEffekt = SKEmitterNode(fileNamed: "Fire")
    
    override func didMove(to view: SKView) {
       
    
        // Tank
        
        tank.position = CGPoint.zero
        tank.position = CGPoint(x: self.size.width / 2 , y: 70)
            // Größe
        tank.setScale(0.1)
        tank.zPosition = 3
        tank.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: tank.size.width, height: tank.size.height))
        tank.physicsBody?.affectedByGravity = false
        tank.physicsBody?.isDynamic = false
        tank.physicsBody?.categoryBitMask = physicsCA.tankPH
        tank.physicsBody?.contactTestBitMask = physicsCA.enemyPH
        tank.lightingBitMask = 0b1
        
        
        self.addChild(tank)
        
        
        // Scene physics 
       //  self.physicsWorld.gravity = CGVector(dx: 0, dy: -9.8)
        
        self.physicsWorld.contactDelegate = self
        
        
        // Audio
        
       // audioURL = Bundle.main.url(forResource: "Background", withExtension: "mp3")
        
        
        // Hintergrund
        
        // self.backgroundColor = SKColor.brown
        
        
        // Herz 
        
        addLive(liveCount: 3)
        
        // ScoreLabel
        
        scoreLabel.fontColor = SKColor.black
        scoreLabel.fontSize = 60.0
        scoreLabel.zPosition = 7
        scoreLabel.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        self.addChild(scoreLabel)

        
        
        // Background
        
        background.anchorPoint = CGPoint(x: 0, y: 0)
        background.position = CGPoint(x: 0, y: 0)
        background.size = self.size
        background.zPosition = 0
        background.lightingBitMask = 0b1
        
        self.addChild(background)
        
        // Pause Background
        

        let pauseBackground = SKSpriteNode(color: SKColor(red: 0, green: 0, blue: 0, alpha: 0.8 ),size: CGSize(width: self.size.width, height: self.size.height))
        pauseBackground.anchorPoint = CGPoint.zero
        pauseBackground.name = "paBack"
        pauseBackground.zPosition = 4
        pauseBackground.lightingBitMask = 0b1
        self.addChild(pauseBackground)
        
        let label = SKLabelNode(text: "BEREIT?")
        label.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2 + 200)
        label.fontSize = 80
        label.fontColor = SKColor.white
        label.zPosition = 10
        label.name = "label"
        self.addChild(label)
        
        let labelLevel = SKLabelNode(text: "für Level 1")
        labelLevel.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2 + 100)
        labelLevel.fontSize = 80
        labelLevel.fontColor = SKColor.white
        labelLevel.zPosition = 10
        labelLevel.name = "label1"
        
        self.addChild(labelLevel)
        
        self.isPaused = true
        
        
        
    }
    
    // Schuss
    
    @objc func spaceshipFire() {
            let spacefire = SKSpriteNode(imageNamed: "Sternschuss")
            spacefire.position = tank.position
            spacefire.zPosition = 2
            spacefire.physicsBody = SKPhysicsBody(circleOfRadius: spacefire.size.width / 2)
            spacefire.physicsBody?.affectedByGravity = false
            spacefire.physicsBody?.isDynamic = false
            spacefire.physicsBody?.categoryBitMask = physicsCA.spacefirePH
            spacefire.physicsBody?.contactTestBitMask = physicsCA.enemyPH


        
        
        // Action 
        
        let fireMove = SKAction.moveTo(y: self.size.height + spacefire.size.height, duration: 4.0)
        let delete = SKAction.removeFromParent()
        
        spacefire.run(SKAction.sequence([fireMove,delete]))
        
        self.addChild(spacefire)
    }
    
    
    
    
    
        // Gegner
    
    @objc func addenemy() {
        let enemy = SKSpriteNode(imageNamed: "Spaceship")
        enemy.position = CGPoint(x: CGFloat(arc4random_uniform(UInt32(self.size.width - 2 * enemy.size.width))) + enemy.size.width ,y: self.size.height + enemy.size.height)
        enemy.zPosition = 2
        enemy.zRotation = CGFloat(Double.pi * 180) / 180
        enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: enemy.size.width, height: enemy.size.height))
        enemy.physicsBody?.affectedByGravity = false
        enemy.physicsBody?.categoryBitMask = physicsCA.enemyPH
        enemy.physicsBody?.contactTestBitMask = physicsCA.spacefirePH | physicsCA.spacefirePH
        enemy.lightingBitMask = 0b1
        enemy.shadowCastBitMask = 0b1
        
        // Action 
        
        let moveDown = SKAction.moveTo(y: -enemy.size.height, duration: 2.0)
        let delete = SKAction.removeFromParent()
        
        enemy.run(SKAction.sequence([moveDown,delete]))
        self.addChild(enemy)
        
    
    }
    
   
    
    func addLive(liveCount: Int) {
        for index in 1...liveCount{
            let herz = SKSpriteNode(imageNamed:"Herz")
            herz.position = CGPoint(x: CGFloat(index) * herz.size.width, y: self.size.height - herz.size.height - 15)
            herz.zPosition = 3
            herz.name = "Herz\(index)"
            liveArray.append(herz)
            self.addChild(herz)
    
    }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let contactA: SKPhysicsBody
        let contactB: SKPhysicsBody
        // let contactC: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.contactTestBitMask {
            contactA = contact.bodyA // Tank  // Spacefire
            contactB = contact.bodyB // Enemy
        } else{
        
            contactA = contact.bodyB  // Tank  // Spacefire
            contactB = contact.bodyA  // Enemy
        }
        
        // 1. Kontakt: Spacefire -Enemy
        if contactA.categoryBitMask == physicsCA.spacefirePH && contactB.categoryBitMask == physicsCA.enemyPH{
           spaceFireContactWhitEnemy(spacefire: contactA.node as! SKSpriteNode, enemy: contactB.node as! SKSpriteNode)
        }
        
        // 2. Kontakt: Tank -Enemy
        if contactA.categoryBitMask == physicsCA.tankPH && contactB.categoryBitMask == physicsCA.enemyPH{
            contactB.node?.removeFromParent()
            
            tank.run(SKAction.repeat(SKAction.sequence([SKAction.fadeAlpha(to: 0.1, duration: 0.1), SKAction.fadeAlpha(to: 1.0, duration: 0.1)]), count: 10))
            
                if let herz1 = self.childNode(withName: "Herz1"){
                    herz1.removeFromParent()
            
                }else if let herz2 = self.childNode(withName: "Herz2"){
                    herz2.removeFromParent()
                }else if let herz3 = self.childNode(withName: "Herz3"){
                    herz3.removeFromParent()
                    tankContactWithEnemy()
            }
            
        }
        
    }
    
    func tankContactWithEnemy(){
        tank.removeFromParent()
       //  backgroundAudio.stop()
        timer6.invalidate()
        timer7.invalidate()
        
        let gameOver = GameOver(size: self.size)
        let transition = SKTransition.flipHorizontal(withDuration: 2.0)
        
        self.view?.presentScene(gameOver, transition: transition)
        
    
    }
    
    func level2() {
        
        let gamescene2 = GameScene2(size: self.size)
        let skview = self.view!
        
        skview.presentScene(gamescene2)
        
    }
    
    
    func spaceFireContactWhitEnemy(spacefire: SKSpriteNode, enemy: SKSpriteNode){
        spacefire.removeFromParent()
        enemy.removeFromParent()
        
        score+=1
        scoreLabel.text = "\(score)"

        UserDefaults.standard.set(score, forKey: "RecentScore")
        if score > UserDefaults.standard.integer(forKey: "Highscore") {
            UserDefaults.standard.set(score, forKey: "Highscore")
        }

        if score == 50 {
            self.level2()
        }
        
        self.run(SKAction.playSoundFileNamed("Explosion+1.wav", waitForCompletion: false))
        let explosion = SKEmitterNode(fileNamed: "explosion.sks")
        explosion?.position = enemy.position
        explosion?.zPosition = 2
        self.addChild(explosion!)
        
        self.run(SKAction.wait(forDuration: 2)) {
            explosion?.removeFromParent()
        }
        
    }
    
    

    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let locationUser = touch.location(in: self)
            tank.position.x = locationUser.x
        
        }
    }
   
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        if self.isPaused == true {
        self.isPaused = false
            
            let backgroundPA = self.childNode(withName: "paBack") as! SKSpriteNode
            let label = self.childNode(withName: "label") as! SKLabelNode
            let level1 = self.childNode(withName: "label1") as!SKLabelNode
            
            backgroundPA.removeFromParent()
            label.removeFromParent()
            level1.removeFromParent()

            
            timer6 = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(GameScene.spaceshipFire), userInfo: nil, repeats: true)
            timer7 = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(GameScene.addenemy), userInfo: nil, repeats: true)
    }
}

    override func update(_ currentTime: TimeInterval) {
        
        light.position.x += 0.5
        if light.position.x > self.size.width {
            light.position.x = 0
            
        }
        
    }
    

}

