//
//  GameViewController.swift
//  TankGunner
//
//  Created by Mirko Jurisch on 04.02.17.
//  Copyright © 2017 Mirko Jurisch. All rights reserved.
//

import UIKit
import SpriteKit


class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let gameScene = Hauptmenue(size: self.view.bounds.size)
        
        let skview = self.view as! SKView
        skview.showsFPS = false
        skview.showsNodeCount = false
        // skview.showsPhysics = true
        skview.presentScene(gameScene)
 
        
        /*
        if let view = self.view as! SKView? {
            if let scene = SKScene(fileNamed: "GameScene") {
        scene.scaleMode = .aspectFill
                view.ignoresSiblingOrder = true
                
                view.showsFPS = true
                view.showsNodeCount = true
        */
        
        }
    }
