//
//  GameOver.swift
//  TankGunner
//
//  Created by Mirko Jurisch on 05.02.17.
//  Copyright © 2017 Mirko Jurisch. All rights reserved.
//

import SpriteKit

    let gameOverLabel = SKLabelNode(text: "Gameover")
   // let scoreLabelgesamt = SKLabelNode(text: "letzte Punktezahl: " + "\(UserDefaults.standard.integer(forKey: "RecentScore"))")

class GameOver: SKScene {

    override func didMove(to view: SKView) {

        gameOverLabel.fontColor = SKColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
        gameOverLabel.fontName = "AvenirNext-Bold"
        gameOverLabel.fontSize = 50
        gameOverLabel.zPosition = 1
        gameOverLabel.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2 + 150)
        

        // scoreLabelgesamt.fontColor = SKColor(red: 230/126, green: 126/255, blue: 34/255, alpha: 1.0)
        // scoreLabelgesamt.fontName = "AvenirNext-Bold"
        // scoreLabelgesamt.fontSize = 30
        // scoreLabelgesamt.zPosition = 1
        // scoreLabelgesamt.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2 )


        
        self.addChild(gameOverLabel)
       // self.addChild(scoreLabelgesamt)
       
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let newGameSzene = Hauptmenue(size: self.size)
        self.view?.presentScene(newGameSzene)
    }
}
