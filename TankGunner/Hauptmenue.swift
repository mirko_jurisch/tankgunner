//
//  Hauptmenue.swift
//  TankGunner
//
//  Created by Mirko Jurisch on 18.03.17.
//  Copyright © 2017 Mirko Jurisch. All rights reserved.
//

import SpriteKit

class Hauptmenue: SKScene{
    
    let gameLabel = SKLabelNode(text: "TankGunner")
    let startLabel = SKLabelNode(text: "Start")
    let highscoreLabel = SKLabelNode(text: "HighScore: ")
    
    
    
    override func didMove(to view: SKView) {
        self.backgroundColor = UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)

        addLabels()
        addLogo()
    }

    func addLogo() {
        let logo = SKSpriteNode(imageNamed: "tank-icon-0")
        logo.size = CGSize(width: frame.size.width/4, height: frame.size.width/4)
        logo.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(logo)
    }



    func addLabels(){
        gameLabel.fontColor = SKColor(red: 230/126, green: 126/255, blue: 34/255, alpha: 1.0)
        // startLabel.fontColor = SKColor(red: 230/126, green: 126/255, blue: 34/255, alpha: 1.0)

        gameLabel.fontName = "AvenirNext-Bold"
        gameLabel.fontSize = 40
        startLabel.fontName = "AvenirNext-Bold"
        startLabel.fontSize = 40

        gameLabel.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2 + 200 )
        startLabel.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2 + 100)

        let highscoreLabel = SKLabelNode(text: "Highscore: " + "\(UserDefaults.standard.integer(forKey: "Highscore"))")
        highscoreLabel.fontName = "AvenirNext-Bold"
        highscoreLabel.fontSize = 25
        highscoreLabel.fontColor = UIColor.white
        highscoreLabel.position = CGPoint(x: frame.midX, y: frame.midY - highscoreLabel.frame.size.height*4)
        addChild(highscoreLabel)

        let recentScoreLabel = SKLabelNode(text: "letzte Punktezahl: " + "\(UserDefaults.standard.integer(forKey: "RecentScore"))")
        recentScoreLabel.fontName = "AvenirNext-Bold"
        recentScoreLabel.fontSize = 25
        recentScoreLabel.fontColor = UIColor.white
        recentScoreLabel.position = CGPoint(x: frame.midX, y: highscoreLabel.position.y - recentScoreLabel.frame.size.height*2)
        addChild(recentScoreLabel)


        self.addChild(gameLabel)
        self.addChild(startLabel)
        animate(label: startLabel)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let locationUser = touch.location(in: self)
            
            if atPoint(locationUser) == startLabel {
                let gamescene = GameScene(size: self.size)
                let skview = self.view!
                
                skview.presentScene(gamescene)
            }
         
        }
    }

    func animate(label: SKLabelNode) {
        let fadeOut = SKAction.fadeOut(withDuration: 1.0)
        let fadeIn = SKAction.fadeIn(withDuration: 1.0)
        let sequence = SKAction.sequence([fadeOut, fadeIn])
        label.run(SKAction.repeatForever(sequence))
    }

}
